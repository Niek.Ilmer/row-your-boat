// Define CPU speed for delay.h
#define F_CPU 8000000UL
#define BAUD 9600

// Includes
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include "USART.c"

// Defines
//#define DEBUGGING

#define SERVODDR DDRA
#define SERVOPORT PORTA
#define SERVOPIN1 PORTA1
#define SERVOPIN2 PORTA2
#define SERVOPIN3 PORTA3
#define SERVOPIN4 PORTA7
#define LEDPORT PORTA
#define LEDPIN PORTA0
#define THRSTATE PINB1
#define STRSTATE PINB0
#define BUTTONPORT PORTB
#define BUTTONDDR DDRB
#define BUTTONPIN PORTB2
#define BUTTONIN PINB
#define BUTTONPULLUP PUEB

#define CALIBRATIONBLINKSPEED 8

#define THRMINADRESS (uint16_t*)0
#define THRMAXADRESS (uint16_t*)2
#define THRMIDADRESS (uint16_t*)24
#define STRMINADRESS (uint16_t*)4
#define STRMAXADRESS (uint16_t*)6
#define STRMIDADRESS (uint16_t*)26
#define SERVO1MINADRESS (uint16_t*)8
#define SERVO1MAXADRESS (uint16_t*)10
#define SERVO2MINADRESS (uint16_t*)12
#define SERVO2MAXADRESS (uint16_t*)14
#define SERVO3MINADRESS (uint16_t*)16
#define SERVO3MAXADRESS (uint16_t*)18
#define SERVO4MINADRESS (uint16_t*)20
#define SERVO4MAXADRESS (uint16_t*)22
#define MAXSPEEDADDRESS (uint8_t*)28

// Variable declaration
int16_t volatile debug = 0;
int16_t volatile test = 0;
int16_t volatile test2 = 0;
uint8_t volatile servoLoop = 0;
uint8_t volatile receiveCycle = 0;
uint8_t volatile timeOutCounter1 = 0;
uint8_t volatile enabled1 = 1;
uint8_t volatile timeOutCounter2 = 0;
uint8_t volatile enabled2 = 1;
uint8_t volatile timeOutCounter3 = 0;
uint8_t volatile enabled3 = 1;
uint8_t volatile timeOutCounter4 = 0;
uint8_t volatile enabled4 = 1;
uint8_t volatile receiveTimeout = 0;
uint8_t volatile receiveEnable = 0;
int8_t volatile receiveThreshold = 10;
uint16_t volatile servo1 = 1500;
uint16_t volatile servo2 = 1501;
uint16_t volatile servo3 = 1502;
uint16_t volatile servo4 = 1503;
uint16_t volatile servo1_old = 1500;
uint16_t volatile servo2_old = 1501;
uint16_t volatile servo3_old = 1502;
uint16_t volatile servo4_old = 1503;
uint16_t volatile thr = 0;
uint16_t volatile str = 0;
uint16_t volatile tempthr = 0;
uint16_t volatile tempstr = 0;
uint8_t volatile servo1Pos = 127;
uint8_t volatile servo2Pos = 0;
uint8_t volatile servo3Pos = 127;
uint8_t volatile servo4Pos = 127;
int8_t volatile servo1MaxLim = 0;
int8_t volatile servo1MinLim = 0;
int8_t volatile servo2MaxLim = 1;
int8_t volatile servo2MinLim = 0;
int8_t volatile servo3MaxLim = 0;
int8_t volatile servo3MinLim = 0;
int8_t volatile servo4MaxLim = 1;
int8_t volatile servo4MinLim = 0;
int8_t volatile maxSpeed = 3;

// Max values

uint16_t volatile thrMin = 1000;
uint16_t volatile thrMax = 2000;
uint16_t volatile thrMid = 1500;
uint16_t volatile strMin = 1000;
uint16_t volatile strMax = 2000;
uint16_t volatile strMid = 1500;
uint16_t volatile servo1Min = 1000;
uint16_t volatile servo1Max = 2000;
uint16_t volatile servo2Min = 1000;
uint16_t volatile servo2Max = 2000;
uint16_t volatile servo3Min = 1000;
uint16_t volatile servo3Max = 2000;
uint16_t volatile servo4Min = 1000;
uint16_t volatile servo4Max = 2000;

// Credits



// Initiation functions
static inline void initiateTimer1(){
	
	// Timer 1
	// Set timer 1 to fast PWM
//	TCCR1A |= (1 << WGM11);
	TCCR1B |= (1 << WGM12) | (1 << WGM13);
	// Set top
	ICR1 = 10000;
	// Set clock
	TCCR1B |= (1 << CS11);
	// Enable compare interrupts
	TIMSK1 |= (1 << OCIE1B) | (1 << OCIE1A) | (1 << ICIE1);
	// Initialize compare values
	OCR1A = 1500;
	OCR1B = 1600;
}

static inline void initiateTimer0(){
	TCCR0B |= (1 << CS01);
	TIMSK0 |= (1 << TOIE0);
	PCMSK1 |= (1 << PCINT9);
	GIMSK  |= (1 << PCIE1);
}

static inline void initiateTimer2(){
	TCCR2B |= (1 << CS20) | (1 << CS22) | (1 << WGM22);
	OCR2A = 7812; // 1hz
	TIMSK2 |= 1 << OCIE2A;
}


static inline void initiatePins(){
	DDRA |= (1 << SERVOPIN1) | (1 << SERVOPIN2) | (1 << SERVOPIN3) | (1 << SERVOPIN4) | (1 << LEDPIN);
	BUTTONDDR &= ~(1 << BUTTONPIN);
	BUTTONPULLUP |= 1 << BUTTONPIN;
}
	
uint8_t checkButton(){ // This function is for use in the calibration
	uint8_t i = 0;
	while(1){
	if (bit_is_clear(BUTTONIN, BUTTONPIN)){
		i = 0;
		while(bit_is_clear(BUTTONIN, BUTTONPIN) && i < 40){
		_delay_us(500);
		i++;
		}
		if (bit_is_clear(BUTTONIN, BUTTONPIN) && i >= 39){
			return 1;
		}
	}
	if (bit_is_set(BUTTONIN, BUTTONPIN)){
		i = 0;
		while(bit_is_set(BUTTONIN, BUTTONPIN) && i < 40){
		_delay_us(500);
		i++;
		}
		if (bit_is_set(BUTTONIN, BUTTONPIN) && i >= 39){
			return 0;
		}
	}
	}
}

void waitUntilButtonPressed(){
	while(!checkButton()){
	}	
}

void waitUntilButtonReleased(){
	while(checkButton()){
	}	
}






inline void initiateEEPROM(uint8_t write){ // This function will write standard values to eeprom on first boot and reads in existing values 
	if (eeprom_read_word(SERVO1MINADRESS) > 5000 || write){
		eeprom_write_word(THRMINADRESS, thrMin);
		eeprom_write_word(THRMAXADRESS, thrMax);
		eeprom_write_word(THRMIDADRESS, thrMid);
		eeprom_write_word(STRMINADRESS, strMin);
		eeprom_write_word(STRMAXADRESS, strMax);
		eeprom_write_word(STRMIDADRESS, strMid);
		eeprom_write_word(SERVO1MINADRESS, servo1Min);
		eeprom_write_word(SERVO1MAXADRESS, servo1Max);
		eeprom_write_word(SERVO2MINADRESS, servo2Min);
		eeprom_write_word(SERVO2MAXADRESS, servo2Max);
		eeprom_write_word(SERVO3MINADRESS, servo3Min);
		eeprom_write_word(SERVO3MAXADRESS, servo3Max);
		eeprom_write_word(SERVO4MINADRESS, servo4Min);
		eeprom_write_word(SERVO4MAXADRESS, servo4Max);
		eeprom_write_byte(MAXSPEEDADDRESS, maxSpeed);
	}
	thrMin = eeprom_read_word(THRMINADRESS);
	thrMax = eeprom_read_word(THRMAXADRESS);
	thrMid = eeprom_read_word(THRMIDADRESS);
	strMin = eeprom_read_word(STRMINADRESS);
	strMax = eeprom_read_word(STRMAXADRESS);
	strMid = eeprom_read_word(STRMIDADRESS);
	servo1Min = eeprom_read_word(SERVO1MINADRESS);
	servo1Max = eeprom_read_word(SERVO1MAXADRESS);
	servo2Min = eeprom_read_word(SERVO2MINADRESS);
	servo2Max = eeprom_read_word(SERVO2MAXADRESS);
	servo3Min = eeprom_read_word(SERVO3MINADRESS);
	servo3Max = eeprom_read_word(SERVO3MAXADRESS);
	servo4Min = eeprom_read_word(SERVO4MINADRESS);
	servo4Max = eeprom_read_word(SERVO4MAXADRESS);
	maxSpeed = eeprom_read_byte(MAXSPEEDADDRESS);
}


// Servo ISRs

ISR(TIMER1_COMPA_vect){  // Servo driving ISR part 1
	switch (servoLoop){
	case (0):
		SERVOPORT &= ~(1 << SERVOPIN1);
		break;
	case (1):
		SERVOPORT &= ~(1 << SERVOPIN3);
		break;
	}
}

ISR(TIMER1_COMPB_vect){  // Servo driving ISR part 2
	switch (servoLoop){
	case (0):
		SERVOPORT &= ~(1 << SERVOPIN2);
		break;
	case (1):
		SERVOPORT &= ~(1 << SERVOPIN4);
		break;
	}
}

ISR(TIMER1_CAPT_vect){  // ISR for figuring out what servo pins to drive high and what servos should time out
	switch (servoLoop){
	case (0):
		servoLoop = 1;
		if (enabled3){
			SERVOPORT |= (1 << SERVOPIN3);
		}
		if (enabled4){
			SERVOPORT |= (1 << SERVOPIN4);
		}
		OCR1A = servo3;
		OCR1B = servo4;
		break;		
	case (1):
		servoLoop = 0;
		if (enabled1){
			SERVOPORT |= (1 << SERVOPIN1);
		}
		if (enabled2){
			SERVOPORT |= (1 << SERVOPIN2);
		}
		OCR1A = servo1;
		OCR1B = servo2;
		break;
	}
	if ((servo1 == servo1_old)){
		if (timeOutCounter1 < 0xFE){
			timeOutCounter1++;
		} else {
			enabled1 = 0;
		}
	}else{
		timeOutCounter1 = 0;
		enabled1 = 1;
	}
	if ((servo2 == servo2_old)){
		if (timeOutCounter2 < 0xFE){
			timeOutCounter2++;
			} else {
			enabled2 = 0;
		}
		}else{
		timeOutCounter2 = 0;
		enabled2 = 1;
	}
	if ((servo3 == servo3_old)){
		if (timeOutCounter3 < 0xFE){
			timeOutCounter3++;
			} else {
			enabled3 = 0;
		}
		}else{
		timeOutCounter3 = 0;
		enabled3 = 1;
	}
	if ((servo4 == servo4_old)){
		if (timeOutCounter4 < 0xFE){
			timeOutCounter4++;
			} else {
			enabled4 = 0;
		}
		}else{
		timeOutCounter4 = 0;
		enabled4 = 1;
	}
	
	servo1_old = servo1;
	servo2_old = servo2;
	servo3_old = servo3;
	servo4_old = servo4;
}

// Receiver ISR

ISR(PCINT1_vect){
	if((receiveCycle == 0) && ( bit_is_set(PINB , THRSTATE ))){
		TIFR0 = 0;
		TCNT0 = 0;
		tempstr = 0;
		tempthr = 0;
		receiveCycle = 1;
		receiveTimeout = 0;
	}
	if((receiveCycle == 1) && ( bit_is_clear(PINB , THRSTATE ))){
		TIFR0 = 0;
		thr = (thr + (tempthr + TCNT0)) >> 1;
		receiveCycle = 2;
		receiveEnable = 1;
// 		if ((thr > (thrMax + 30)) || (thr < (thrMin - 30))){
// 			receiveEnable = 0;
// 		}
		PCMSK1 &= ~(1 << PCINT9);
		PCMSK1 |= (1 << PCINT8);
	}
	if((receiveCycle == 2) && ( bit_is_set(PINB , STRSTATE ))){
		TIFR0 = 0;
		TCNT0 = 0;
		tempstr = 0;
		tempthr = 0;
		receiveCycle = 3;
	}
	if((receiveCycle == 3) && ( bit_is_clear(PINB , STRSTATE ))){
		TIFR0 = 0;
		str = (str + (tempstr + TCNT0)) >> 1;
		receiveCycle = 0;
		receiveEnable = 1;
// 		if ((str > (strMax + 30)) || (str < (strMin - 30))){
// 			receiveEnable = 0;
// 		}
		PCMSK1 &= ~(1 << PCINT8);
		PCMSK1 |= (1 << PCINT9);
	}
	GIFR = 0;
}

ISR(TIMER0_OVF_vect){
	TIFR0 = 0;
	tempstr = (tempstr + 255);
	tempthr = (tempthr + 255);
	if (receiveTimeout <255){
		receiveTimeout++;
	}else{
		receiveEnable = 0;
	}
}

ISR(TIMER2_COMPA_vect){
	LEDPORT ^= 1 << LEDPIN;
}

int32_t mapVal(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max)
{
	int32_t min;
	int32_t max;
	
  if (in_min < in_max){
	  min = in_min;
	  max = in_max;
  } else {	  
	  min = in_max;
	  max = in_min;
  }
  
  if (x < min){
	  x = min;
  } else if (x > max){
	  x = max;
  }

  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void setservo1(long val){
	servo1 = mapVal(val, 0, 255, servo1Min, servo1Max);
}
void setservo2(long val){
	servo2 = mapVal(val, 0, 255, servo2Min, servo2Max);
}
void setservo3(long val){
	servo3 = mapVal(val, 0, 255, servo3Min, servo3Max);
}
void setservo4(long val){
	servo4 = mapVal(val, 0, 255, servo4Min, servo4Max);
}

uint8_t getServo1(){
	return mapVal(servo1, servo1Min, servo1Max, 0, 255);
}
uint8_t getServo2(){
	return mapVal(servo2, servo2Min, servo2Max, 0, 255);
}
uint8_t getServo3(){
	return mapVal(servo3, servo3Min, servo3Max, 0, 255);
}
uint8_t getServo4(){
	return mapVal(servo4, servo4Min, servo4Max, 0, 255);
}

int8_t mapThr(){
	/*if (((thrMid < thrMax) && (thr > (thrMid + 100))) || ((thrMid > thrMax) && (thr < (thrMid - 100)))){
		long val = mapVal(thr, thrMid, thrMax, 0, 120);
		if(val > 127){
			return (127);
		}
		if(val < 0){
			return (0);
		}
		return val;
	}
	if (((thrMid > thrMin) && (thr < (thrMid - 100))) || ((thrMid < thrMax) && (thr > (thrMid + 100)))){
		int32_t val = mapVal(thr, thrMin, thrMid, -120, 0);
		if(val > 0){
			return (0);
		}
		if(val < -127){
			return (-127);
		}
		return val;
	}*/
	int8_t ret = mapVal(thr , thrMin, thrMax, -127, 127);
	if ((ret < 15 && ret > -15) || (thr > (thrMid - 100) && thr < (thrMid + 100))) {
		ret = 0;
	}
	
	return ret;
}

int8_t mapStr(){
// 	if (((strMid < strMax) && (str > (strMid + 50))) || ((strMid > strMax) && (str < (strMid - 50)))){
// 		long val = mapVal(str, strMid, strMax, 0, 120);
// 		if(val > 127){
// 			return (127);
// 		}
// 		if(val < 0){
// 			return (0);
// 		}
// 		return val;
// 	}
// 	if (((strMid > strMin) && (str < (strMid - 50))) || ((strMid < strMax) && (str > (strMid + 50)))){
// 		int32_t val = mapVal(str, strMin, strMid, -120, 0);
// 		if(val > 0){
// 			return (0);
// 		}
// 		if(val < -127){
// 			return (-127);
// 		}
// 		return val;
// 	}
// 	return (0);

	int8_t ret = mapVal(str , strMin, strMax, -127, 127);
	if ((ret < 15 && ret > -15) || (str > (strMid - 100) && str < (strMid + 100))) {
		ret = 0;
	}
	
	return ret;
}


#ifdef DEBUGGING
void printServoToScreen(){
	printString("sd0,0;");
	_delay_ms(5);
	printString("ssS1:");
	printServo(servo1);
	printString(" S2:");
	printServo(servo2);
	printString(";");
	_delay_ms(5);
	printString("sd1,0;");
	_delay_ms(5);
	printString("ssS3:");
	printServo(servo3);
	printString(" S4:");
	printServo(servo4);
	printString(" ;");
	_delay_ms(5);
}
#endif

// Following function is courtesy of Mito IT and Ilmer engineering
void leftPeddal (int8_t thrReceived, int8_t strReceived) {
	if ((thrReceived == 0 && strReceived == 0) || (strReceived > 0)) {
		uint8_t resetSpeed = maxSpeed;
		
		servo1Pos = servo2Pos == 255 ? (servo1Pos > (127 + resetSpeed) ? servo1Pos - resetSpeed : (servo1Pos < (127 - resetSpeed) ? servo1Pos + resetSpeed : 127)) : servo1Pos;
		servo2Pos = servo2Pos < 255 ? ((servo2Pos + resetSpeed) > 255 ? 255 : servo2Pos + resetSpeed) : 255;
		
		return;
	}
	
	thrReceived = strReceived < 0 && thrReceived == 0 ? -100 : thrReceived;
	int8_t thrMap = mapVal(thrReceived, -127, 127, -maxSpeed, maxSpeed);
	int8_t thrMapVer = thrMap * 2; // could be changed with maxspeed;
	
	int8_t servo1MaxLimAft = servo2Pos >= 240 ? 1 : 0;
	int8_t servo1MinLimAft = servo2Pos <= 15 ? 1 : 0;
	int8_t servo2MaxLimAft = servo1Pos >= 240 ? 1 : 0;
	int8_t servo2MinLimAft = servo1Pos <= 15 ? 1 : 0;
	
	if (servo1MaxLimAft == 1 || servo1MinLimAft == 1 || servo2MaxLimAft == 1 || servo2MinLimAft == 1) {
		servo1MaxLim = servo2Pos >= 240 ? 1 : 0;
		servo1MinLim = servo2Pos <= 15 ? 1 : 0;
		servo2MaxLim = servo1Pos >= 240 ? 1 : 0;
		servo2MinLim = servo1Pos <= 15 ? 1 : 0;
	}
	
	servo1Pos = servo2Pos <= 15 ? 
					((int)servo1Pos - thrMap <= 0 ? 0 : 
						((int)servo1Pos - thrMap >= 255 ? 255 : servo1Pos - thrMap)) : 
				(servo2Pos >= 240 ? 
					((int)servo1Pos + thrMap >= 255 ? 255 : 
						((int)servo1Pos + thrMap <= 0 ? 0 : servo1Pos + thrMap)) : servo1Pos);
	
	servo2Pos = servo1Pos >= 240 ? 
					((int)servo2Pos - thrMapVer <= 0 ? 0 : 
						((int)servo2Pos - thrMapVer >= 255 ? 255 : servo2Pos - thrMapVer)) :
				(servo1Pos <= 15 ? 
					((int)servo2Pos + thrMapVer <= 0 ? 0 :
						((int)servo2Pos + thrMapVer >= 255 ? 255 : servo2Pos + thrMapVer)) : servo2Pos);
	
	servo1MaxLimAft = servo2Pos >= 240 ? 1 : 0;
	servo1MinLimAft = servo2Pos <= 15 ? 1 : 0;
	servo2MaxLimAft = servo1Pos >= 240 ? 1 : 0;
	servo2MinLimAft = servo1Pos <= 15 ? 1 : 0;
	
	if (servo1MaxLimAft != 1 && servo1MinLimAft != 1 && servo2MaxLimAft != 1 && servo2MinLimAft != 1) {
		if (servo1MaxLim == 1) {
			servo1Pos = 255;
		}
	
		if (servo1MinLim == 1) {
			servo1Pos = 0;
		}
	
		if (servo2MaxLim == 1) {
			servo2Pos = 255;
		}
	
		if (servo2MinLim == 1) {
			servo2Pos = 0;
		}
	}
}

// Following function is courtesy of Mito IT and Ilmer engineering
void rightPeddal (int8_t thrReceived, int8_t strReceived) {
	if ((thrReceived == 0 && strReceived == 0) || (strReceived < 0)) {
		uint8_t resetSpeed = maxSpeed;
		
		servo3Pos = servo4Pos == 255 ? (servo3Pos > (127 + resetSpeed) ? servo3Pos - resetSpeed : (servo3Pos < (127 - resetSpeed) ? servo3Pos + resetSpeed : 127)) : servo3Pos;
		servo4Pos = servo4Pos < 255 ? ((servo4Pos + resetSpeed) > 255 ? 255 : servo4Pos + resetSpeed) : 255;
		
		return;
	}
	
	thrReceived = strReceived > 0 && thrReceived == 0 ? -100 : thrReceived;
	int8_t thrMap = mapVal(thrReceived, -127, 127, -maxSpeed, maxSpeed);
	int8_t thrMapVer = thrMap * 2;
	
	int8_t servo3MaxLimAft = servo4Pos >= 240 ? 1 : 0;
	int8_t servo3MinLimAft = servo4Pos <= 15 ? 1 : 0;
	int8_t servo4MaxLimAft = servo3Pos >= 240 ? 1 : 0;
	int8_t servo4MinLimAft = servo3Pos <= 15 ? 1 : 0;
	
	if (servo3MaxLimAft == 1 || servo3MinLimAft == 1 || servo4MaxLimAft == 1 || servo4MinLimAft == 1) {
		servo3MaxLim = servo4Pos >= 240 ? 1 : 0;
		servo3MinLim = servo4Pos <= 15 ? 1 : 0;
		servo4MaxLim = servo3Pos >= 240 ? 1 : 0;
		servo4MinLim = servo3Pos <= 15 ? 1 : 0;
	}
	
	servo3Pos = servo4Pos <= 15 ? 
					((int)servo3Pos - thrMap <= 0 ? 0 : 
						((int)servo3Pos - thrMap >= 255 ? 255 : servo3Pos - thrMap)) : 
				(servo4Pos >= 240 ? 
					((int)servo3Pos + thrMap >= 255 ? 255 : 
						((int)servo3Pos + thrMap <= 0 ? 0 : servo3Pos + thrMap)) : servo3Pos);
	
	servo4Pos = servo3Pos >= 240 ? 
					((int)servo4Pos - thrMapVer <= 0 ? 0 : 
						((int)servo4Pos - thrMapVer >= 255 ? 255 : servo4Pos - thrMapVer)) :
				(servo3Pos <= 15 ? 
					((int)servo4Pos + thrMapVer <= 0 ? 0 :
						((int)servo4Pos + thrMapVer >= 255 ? 255 : servo4Pos + thrMapVer)) : servo4Pos);
	
	servo3MaxLimAft = servo4Pos >= 240 ? 1 : 0;
	servo3MinLimAft = servo4Pos <= 15 ? 1 : 0;
	servo4MaxLimAft = servo3Pos >= 240 ? 1 : 0;
	servo4MinLimAft = servo3Pos <= 15 ? 1 : 0;
	
	if (servo3MaxLimAft != 1 && servo3MinLimAft != 1 && servo4MaxLimAft != 1 && servo4MinLimAft != 1) {
		if (servo3MaxLim == 1) {
			servo3Pos = 255;
		}
	
		if (servo3MinLim == 1) {
			servo3Pos = 0;
		}
	
		if (servo4MaxLim == 1) {
			servo4Pos = 255;
		}
	
		if (servo4MinLim == 1) {
			servo4Pos = 0;
		}
	}
}


void blinkLed(uint8_t hz){
	if (hz == 0 || hz == 1){
		TCCR2B &= ~((1 << CS20) | (1 << CS22) | (1 << WGM22));
		OCR2A = 0;
		LEDPORT |= hz << LEDPIN;
		return;
	}
	uint16_t newHz = (uint16_t)(7812 * 2)/(uint16_t)hz;
	if (OCR2A != newHz){
		TCCR2B |= (1 << CS20) | (1 << CS22) | (1 << WGM22);
		TCNT2 = 0;
		OCR2A = newHz;
	}
}

void calibrateServoLimit(volatile uint16_t *servo, volatile uint16_t *limit){
	while (!checkButton()){
		int8_t received = mapThr();
		if (received > 50){
			*servo = *servo + 10;
			_delay_ms(10);
		}
		if (received < -50){
			*servo = *servo - 10;
			_delay_ms(10);
		}
	}
	*limit = *servo;
	blinkLed(CALIBRATIONBLINKSPEED);
	waitUntilButtonReleased();
	_delay_ms(1000);
	blinkLed(1);
}

void calibration(){
	blinkLed(CALIBRATIONBLINKSPEED);
	waitUntilButtonReleased();
	_delay_ms(1000);
	blinkLed(1);
	waitUntilButtonPressed();
	blinkLed(CALIBRATIONBLINKSPEED);
	thrMid = thr;
	waitUntilButtonReleased();
	_delay_ms(1000);
	blinkLed(1);
	waitUntilButtonPressed();
	blinkLed(CALIBRATIONBLINKSPEED);
	thrMin = thr; // reversed
	waitUntilButtonReleased();
	_delay_ms(1000);
	blinkLed(1);
	waitUntilButtonPressed();
	blinkLed(CALIBRATIONBLINKSPEED);
	thrMax = thr; // reversed
	waitUntilButtonReleased();
	_delay_ms(1000);
	blinkLed(1);
	waitUntilButtonPressed();
	blinkLed(CALIBRATIONBLINKSPEED);
	strMid = str; 
	waitUntilButtonReleased();
	_delay_ms(1000);
	blinkLed(1);
	waitUntilButtonPressed();
	blinkLed(CALIBRATIONBLINKSPEED);
	strMin = str; 
	waitUntilButtonReleased();
	_delay_ms(1000);
	blinkLed(1);
	waitUntilButtonPressed();
	blinkLed(CALIBRATIONBLINKSPEED);
	strMax = str; 
	waitUntilButtonReleased();
	_delay_ms(1000);
	blinkLed(1);
	
	calibrateServoLimit(&servo1, &servo1Max);
	calibrateServoLimit(&servo1, &servo1Min);
	calibrateServoLimit(&servo2, &servo2Min);
	calibrateServoLimit(&servo2, &servo2Max);
	calibrateServoLimit(&servo3, &servo3Max);
	calibrateServoLimit(&servo3, &servo3Min);
	calibrateServoLimit(&servo4, &servo4Min);
	calibrateServoLimit(&servo4, &servo4Max);
	
	blinkLed(1);
	waitUntilButtonPressed();
	blinkLed(CALIBRATIONBLINKSPEED);
	int8_t thrIn = mapThr();
	if (thrIn < -50){
		maxSpeed = 2;
	} else if (thrIn > 50){
		maxSpeed = 4;
	} else{
		maxSpeed = 3;
	}
	waitUntilButtonReleased();
	_delay_ms(1000);
	
	
	/*
	The calibration sequence is as follows:
	-throttle middle
	-throttle forwards
	-throttle backwards
	-steering middle
	-steering left
	-steering right
	-right paddle forwards
	-right paddle backwards
	-right paddle up
	-right paddle down
	-left paddle forwards
	-left paddle backwards
	-left paddle up
	-left paddle down
	-speed
	*/
}



// Main function
int main(void){
	CCP = 0xD8;
	CLKPR = 0;
	//clock_prescale_set(clock_div_1);		// Set clock to 8MHz
	initiateEEPROM(0);
	initiateTimer1();
	initiateTimer0();
	initiateTimer2();
	initiatePins();
	initUSART();
	sei();
	if (checkButton()){
		calibration();
		initiateEEPROM(1);
	}
	
	#ifdef DEBUGGING
	printString("su1;");
	printString("sc;");
	#endif
	
	
    while (1){
		while (receiveEnable){
			blinkLed(1);
			int8_t volatile thrReceived = mapThr();
			int8_t volatile strReceived = mapStr();
			test = thrReceived;
			test2 = strReceived;
			LEDPORT |= 1 << LEDPIN;
			
			leftPeddal(thrReceived, strReceived);
			rightPeddal(thrReceived, strReceived);
			
			setservo1(servo1Pos);	// Horizontal left, front 255, back 0
			setservo2(servo2Pos);	// Vertical left, up 255, down 0
			setservo3(servo3Pos);	// Horizontal right, front 255, back 0
			setservo4(servo4Pos);	// Vertical right, up 255, down 0
			
			#ifdef DEBUGGING
 			printServoToScreen();
			#endif
			
			_delay_ms(5);
		}
		blinkLed(15);
	}	
	return(1);
}